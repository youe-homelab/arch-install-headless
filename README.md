# Overview

Minimal arch linux iso for a headless installation using a customised version of the **baseline** profile from the archiso project.

### Features :

- automatic build uploaded to gitlab packages
- ssh enabled with an authorization key
- script retrieval from gitlab snippet


# Usage

1. Flash medium and boot
> Download the latest built from this [link](https://gitlab.com/youe-homelab/arch-install-headless/-/packages)

2. Connect using shh
> Password is set by repository variable : `PASSWORD`

3. Execute Remote Script with `ers` followed by your script name from snippet
> Base url is set by repository variable : `SCRIPT_LOCATION`

# Customize install script

Install script is retrived from the reporitory [snippets](https://gitlab.com/youe-homelab/arch-install-headless/-/snippets).

The project uses gitlab ability to host multiple files into one snippet to customize installation per machine. It is then possible to run `ers home` to setup the 'home' environment and `ers cloud` for remote.

# Resources

| Reference | Description |
| - | - |
| [archlinux](https://archlinux.org) | BTW this project use arch | 
| [archiso](https://gitlab.archlinux.org/archlinux/archiso) | The archiso project used to build the iso |
