#!/usr/bin/env sh
#

if [ -z "$1" ]; then
    printf "Please provide image location\n"
    return 1
fi

if grep -q "vmx\|svm" /proc/cpuinfo; then
    run_archiso -i "$1"
else
    isodir=$(dirname "$1" | xargs realpath)
    xorriso -osirrox on -indev "$1" -extract arch/boot/x86_64 "$isodir"
    ISO_VOLUME_ID=$(xorriso -indev "$1" 2>&1 | awk -F : '$1 ~ "Volume id" {print $2}' | tr -d "' ")

    qemu-system-x86_64 \
        -m "size=3072,slots=0,maxmem=$((3072*1024*1024))" \
        -kernel "$isodir/vmlinuz-linux" \
        -initrd "$isodir/initramfs-linux".img \
        -append "archisobasedir=arch archisolabel=${ISO_VOLUME_ID} console=ttyS0" \
        -device virtio-net-pci,romfile=,netdev=net0 -netdev user,id=net0 \
        -cdrom "$1" \
        -nographic \
        -no-reboot
fi
