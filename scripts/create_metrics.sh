#!/usr/bin/env sh
#

tmpdir="/tmp/archiso/work"

printf 'image_size_mebibytes %s\n' "$(du -m "./out/install.iso" | cut -f1)"
printf 'package_count %s\n' "$(sort -u "${tmpdir}/iso/"*/pkglist.*.txt | wc -l)"
if [ -e "${tmpdir}/efiboot.img" ]; then
  printf 'eltorito_efi_image_size_mebibytes %s\n' "$(du -m "${tmpdir}/efiboot.img" | cut -f1)"
fi
# shellcheck disable=SC2046
# shellcheck disable=SC2183
printf 'initramfs_size_mebibytes{initramfs="%s"} %s\n' \
  $(du -m "${tmpdir}/iso/"*/boot/**/initramfs*.img | awk '
    function basename(file) {
      sub(".*/", "", file)
      return file
    }
    { print basename($2), $1 }')
