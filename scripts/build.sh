#!/usr/bin/env bash
#

basedir=$(dirname "$0" | xargs realpath)
outdir="$basedir/../out"
workdir=/tmp/archiso

# Cleanup
rm -rf "$workdir"
mkdir "$workdir"
mkdir "$outdir"
cd "$workdir" || exit 1

# Prepare profile 
cp -r "$basedir/../profile" ./profile
PASSWORD=$(openssl passwd -6 -salt xyz "$PASSWORD")
replace_keys="
SCRIPT_LOCATION \
PASSWORD \
"
for k in $replace_keys; do
    grep -Ilr "$k" "./profile" | while read -r f; do
        printf "Replacing $%s in %s\n" "$k" "$f"
        sed -i "s,\$$k,${!k},g" "$f"
    done
done

# Build
mkarchiso -v ./profile || exit 1
mv ./out/*.iso "$outdir/install.iso" || exit 1
